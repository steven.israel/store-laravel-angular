'use strict';

/* Controllers */

var storeControllers = angular.module('storeControllers', []);

storeControllers.controller('AppCtrl', ['$scope', 'Log', 'Logout', 'Store',
    function AppCtrl($scope, Log, Logout, Store) {

        // Variables
        $scope.isLogged = false;
        $scope.data = {};
        $scope.page = 1;
        $scope.previous = false;
        $scope.next = false;

        /* Initial log */
        Log.get({},
            function success(response) {
                $scope.isLogged = response.auth;
            },
            function error(errorResponse) {
                console.log("Error:" + JSON.stringify(errorResponse));
            }
        );

        /* Pagination */
        $scope.paginate = function (direction) {
            if (direction === 'previous')
                --$scope.page;
            else if (direction === 'next')
                ++$scope.page;
            Store.get({page: $scope.page},
                function success(response) {
                    $scope.data = response.data;
                    $scope.previous = response.prev_page_url;
                    $scope.next = response.next_page_url;
                    //console.log(response.data);
                },
                function error(errorResponse) {
                    console.log("Error:" + JSON.stringify(errorResponse));
                }
            );
        };

        /* Initial page */
        $scope.paginate();

        /* Logout */
        $scope.logout = function () {
            Logout.get({},
                function success() {
                    $scope.isLogged = false;
                    $.each($scope.data, function(key) {
                        $scope.data[key].is_owner = false;
                    });
                },
                function error(errorResponse) {
                    console.log("Error:" + JSON.stringify(errorResponse));
                }
            );
        };

        /* Edit Store */
        $scope.edit = function (id, index) {
            $scope.errorContent = null;
            $scope.id = $scope.data[index].id;
            $scope.content = $scope.data[index].content;
            $scope.index = index;
            $('#myModal').modal();
        };

        /* Destroy Store  */
        $scope.destroy = function (id) {
            if (confirm("Really delete this Store ?"))
            {
                Store.delete({id: id},
                    function success() {
                        $scope.paginate();
                    },
                    function error(errorResponse) {
                        console.log("Error:" + JSON.stringify(errorResponse));
                    }
                );
            }
        };

        /* Update Store */
        $scope.submitChange = function () {
            $scope.errorContent = null;
            Store.update({id: $scope.id}, {content: $scope.content},
                function success(response) {
                    $scope.data[$scope.index].content = $scope.content;
                    $('#myModal').modal('hide');
                },
                function error(errorResponse) {
                    $scope.errorContent = errorResponse.data.content[0];
                }
            );
        };

    }]);

storeControllers.controller('LoginCtrl', ['$scope', 'Login',
    function LoginCtrl($scope, Login) {
        $scope.isAlert = false;

        /* Login */
        $scope.submit = function () {
            $scope.errorEmail = null;
            $scope.errorPassword = null;
            $scope.isAlert = false;
            Login.save({}, $scope.formData,
                function success(response) {
                    if (response.result === 'success') {
                        $scope.$parent.isLogged = true;
                        $scope.$parent.paginate();
                        window.location = '#page-top';
                    } else {
                        $scope.isAlert = true;
                        if (response.result === 'lockout') {
                            $scope.messageAlert = response.message;
                        } else {
                            $scope.messageAlert = 'These credentials do not match our records.';
                        }
                    }
                },
                function error(errorResponse) {
                    if (errorResponse.data.password) {
                        $scope.errorPassword = errorResponse.data.password[0];
                    }
                    if (errorResponse.data.email) {
                        $scope.errorEmail = errorResponse.data.email[0];
                    }
                }
            );
        };

    }]);

storeControllers.controller('StoreCtrl', ['$scope', 'Store',
    function StoreCtrl($scope, Store) {

        /* Create Store */
        $scope.submitCreate = function () {
            $scope.errorCreateContent = null;
            Store.save({}, $scope.formData,
                function success(response) {
                    $scope.formData.content = null;
                    $scope.$parent.page = 1;
                    $scope.$parent.data = response.data;
                    $scope.$parent.previous = response.prev_page_url;
                    $scope.$parent.next = response.next_page_url;
                    window.location = '#stores';
                },
                function error(errorResponse) {
                    $scope.errorCreateContent = errorResponse.data.content[0];
                }
            );
        };

    }]);



