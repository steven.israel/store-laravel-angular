'use strict';

/* Services */

var storeServices = angular.module('storeServices', ['ngResource']);

storeServices.factory('Log', ['$resource',
    function ($resource) {
        return $resource("log", {}, {
            get: {method: 'GET'}
        });
    }]);

storeServices.factory('Login', ['$resource',
    function ($resource) {
        return $resource("login", {}, {
            save: {method: 'POST'}
        });
    }]);

storeServices.factory('Logout', ['$resource',
    function ($resource) {
        return $resource("logout", {}, {
            get: {method: 'GET'}
        });
    }]);

storeServices.factory('Store', ['$resource',
    function ($resource) {
        return $resource("product/:id", {page: '@page'}, {
            get: {method: 'GET'},
            save: {method: 'POST'},
            delete: {method: 'DELETE'},
            update: {method: 'PUT'}
        });
    }]);




