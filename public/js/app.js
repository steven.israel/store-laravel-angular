'use strict';

/* App Module */

var storeApp = angular.module('storeApp', [
    'storeControllers',
    'storeServices'
    
]);