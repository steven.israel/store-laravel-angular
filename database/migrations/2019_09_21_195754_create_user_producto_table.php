<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_producto', function(Blueprint $table) {
            $table->increments('id_user_producto');
            $table->integer('id_producto')->unsigned();
            $table->char('cantidad');
            $table->integer('estado');
            $table->timestamps();

            $table->foreign('id_producto')->references('id_producto')->on('producto')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_producto', function(Blueprint $table) {
            $table->dropForeign('user_producto_id_producto_foreign');
        });

        Schema::drop('user_producto');
    }
}
