<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Dream;
use App\Producto;

class DatabaseSeeder extends Seeder {

    protected $lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        User::create([
            'name' => 'admin',
            'email' => 'admin@use.fr',
            'password' => bcrypt('admin'),
            'admin' => true
        ]);

        User::create([
            'name' => 'user',
            'email' => 'user@use.fr',
            'password' => bcrypt('user')
        ]);

        Producto::create(
            ['titulo' => 'Aguacate','imagen' => 'aguacate.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'ajo','imagen' => 'ajo.jpg','precio' => 2,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'almendras','imagen' => 'almendras.jpg','precio' => 3.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'arandanos','imagen' => 'arandanos.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'brocoli','imagen' => 'brocoli.png','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'calabaza','imagen' => 'calabaza.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'canela','imagen' => 'canela.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'cebolla','imagen' => 'cebolla.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'fresa','imagen' => 'fresa.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'kiwi','imagen' => 'kiwi.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'limon','imagen' => 'limon.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'lychee','imagen' => 'lychee.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'maiz','imagen' => 'maiz.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'manzana','imagen' => 'manzana.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'naranja','imagen' => 'naranja.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'papa','imagen' => 'papa.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'pasta','imagen' => 'pasta.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'pimienta','imagen' => 'pimienta.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'repollo','imagen' => 'repollo.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'tomate','imagen' => 'tomate.jpg','precio' => 2.5,'unidades' => 10]
        );
        Producto::create(
            ['titulo' => 'zanahoria','imagen' => 'zanahoria.jpg','precio' => 2.5,'unidades' => 10]
        );

        foreach (range(1, 10) as $i) {
            Dream::create([
                'content' => $i . ' ' . $this->lorem,
                'user_id' => rand(1, 2)
            ]);
        }
    }

}
