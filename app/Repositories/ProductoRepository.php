<?php

namespace App\Repositories;

use App\Producto;

class ProductoRepository {

    /**
     * Get products with user paginate.
     *
     * @param  integer $n
     * @return collection
     */
    public function getProductPaginate($n)
    {
        $products = Producto::latest()
                ->simplePaginate($n);

        return $products;
    }
}
