<?php

Route::get('/', function(){
    return view('index');
});

Route::auth();

Route::get('log', 'Auth\AuthController@log');

Route::resource('store', 'DreamController', ['only' => ['index', 'store', 'update', 'destroy']]);

Route::resource('product', 'ProductoController', ['only' => ['index', 'store', 'update', 'destroy']]);


