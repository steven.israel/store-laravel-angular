<?php

namespace App\Http\Controllers;

use App\Repositories\ProductoRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductoController extends Controller
{
    /**
     * Repository instance.
     *
     */
    protected $productRepository;

    /**
     * Create a new DreamController controller instance.
     *
     * @param  App\Repositories\DreamRepository $dreamRepository
     * @return void
     */
    public function __construct(ProductoRepository $productRepository)
    {
        $this->productRepository = $productRepository;

        $this->middleware('auth', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json($this->productRepository->getProductPaginate(4));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\DreamRequest $request
     * @return Response
     */
    public function store(ProductoRepository $request)
    {
        $this->productRepository->store($request->all(), auth()->id());

        return response()->json($this->productRepository->getProductPaginate(4));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\DreamRequest $request
     * @param  int  $id
     * @return Response
     */
    public function update(ProductoRepository $request, $id)
    {
        if ($this->productRepository->update($request->all(), $id)) {
            return response()->json(['result' => 'success']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->productRepository->destroy($id)) {
            return response()->json(['result' => 'success']);
        }
    }

}
